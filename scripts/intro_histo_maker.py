#!/usr/bin/env python2.7

'''
This script makes histograms from the branchers in intro_tree_maker.py
It creates a ROOT file called sample_histo.root
That stores all of the histograms created in this script

'''




# Things to do
# 1. Try to adjust the range of the histos to not have so much blank space
# 2. Add more histograms of different variables, pt ,eta, phi, M, E, Et
# 3. Make more histograms of these variables for other particles


# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"


# fileName = "/cluster/home/amyrewoldt/DAOD/3lep_Hplus500_slep110_Xn165_Xc120/truth1/DAOD_TRUTH1.aod.pool.root"

## Change the file name here, to whatever what output by your ttree flattening file
fileName = "/cluster/home/jcardenas34/new_group_member_introduction/sample_tree.root"


file = ROOT.TFile.Open(fileName, "READONLY")

branch = file.CollectionTree
outF = ROOT.TFile.Open("sample_histo.root", "RECREATE")


# Histos Below #
tau_pt_hist = ROOT.TH1F("tau_pt_hist","tau_pt",100,0,5e6)
jet_pt_hist = ROOT.TH1F("jet_pt_hist","jet_pt",100,0,5e6)





print "Number of input events:", branch.GetEntries()

# Each one of the objects  in the containers, (taus, jets, mets, muons) is a TLorentzVector, so it can only have TLorentzVector fuctions called on it.
for entry in xrange(branch.GetEntries()):
  branch.GetEntry(entry)
  taus_container = branch.taus
  jets_container = branch.jets


  for tau in taus_container:
    # print tau.Pt()
    tau_pt_hist.Fill(tau.Pt())

  for jets in jets_container:
    # print jets.Pt()
    jet_pt_hist.Fill(jets.Pt())


canvas = ROOT.TCanvas()
tau_pt_hist.Draw("HIST")
canvas.Print("tau_pt_hist.pdf")

jet_pt_hist.Draw("HIST")
canvas.Print("jets_pt_hist.pdf")


outF.Write()
outF.Close()







print "Finished"
