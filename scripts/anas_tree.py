#Creating simple root ntuples from the truth derivation files for Amy's 3-lepton analysis
#To modify code for 3-lepton analysis 
  #1. Change the file that program will read from to the location of Amy's file on the tier3
  #2. Save variables form truth electrons and truth muons (4-momentume of the electrons and muons and their charges)

#!/usr/bin/env python2.7

# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

#fileName = "/cluster/home/bburghgr/truth-hstaustau/daod/DAOD_TRUTH3.test.pool.root" #want to pull info from this file
fileName = "/cluster/home/amyrewoldt/DAOD/3lep_Hplus500_slep110_Xn165_Xc120/truth1/DAOD_TRUTH1.aod.pool.root"

f = ROOT.TFile.Open(fileName, "READONLY")
t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")

print "Number of input events:", t.GetEntries()



# Things to do, Add more particle branches
# 1. Try to find all the charged higgs, and add them to a branch called ChargedHiggs
# the pdgId of the charged higgs is 37






outF = ROOT.TFile.Open("someones_tree.root", "RECREATE")
outTree = ROOT.TTree("CollectionTree", "Test TTree")

# Hists #

tau_pt_hist = ROOT.TH1F("tau_pt_hist","tau_pt",100,0,5e6)
jet_pt_hist = ROOT.TH1F("jet_pt_hist","jet_pt",100,0,5e6)



# Create vectors to store tlorentzvectors
# ROOT.vector(ROOT.TLorentzVector)() == std::vector<TLorentzVector>()
taus = ROOT.vector(ROOT.TLorentzVector)()
jets = ROOT.vector(ROOT.TLorentzVector)()
mets = ROOT.vector(ROOT.TLorentzVector)()
muons = ROOT.vector(ROOT.TLorentzVector)()
electrons = ROOT.vector(ROOT.TLorentzVector)()

# Create branches
outTree.Branch("taus", taus)
outTree.Branch("jets", jets)
outTree.Branch("mets", mets)
outTree.Branch("muons", muons)
outTree.Branch("electrons", electrons)



# You want to make a histo, that has a variable entry for each tau, like pt.
# Loop though through tau container or any other container.

for entry in xrange(t.GetEntries()):
  t.GetEntry(entry)
  inTaus = t.TruthTaus
  inJets = t.AntiKt4TruthDressedWZJets
  inMets = t.MET_Truth
  inMuons = t.TruthMuons
  inElectrons = t.TruthElectrons

  taus.clear()
  jets.clear()
  mets.clear()
  muons.clear()
  electrons.clear()

  # Do cuts and overlap removal here ()
  for tau in inTaus:
    taus.push_back(tau.p4())
    tau_pt_hist.Fill(tau.pt())

  for jet in inJets:
    jets.push_back(jet.p4())
    jet_pt_hist.Fill(jet.pt())

  for met in inMets:
    mp4 = ROOT.TLorentzVector()
    mp4.SetPtEtaPhiM(met.met(), 0, met.phi(), 0)
    mets.push_back(mp4)

  for muon in inMuons:
    muons.push_back(muon.p4())  #push_back is a function you can call on a vector that adds a new entry to the vector

  for electron in inElectrons:
      electrons.push_back(electron.p4())

  outTree.Fill()
  #print "DEBUG: Event", entry, "taus:", len(taus), "jets:", len(jets), "mets:", len(mets)


# Plotting can be done in this file, uncomment below to do that.

# canvas = ROOT.TCanvas()
# tau_pt_hist.Draw("HIST")
# canvas.Print("tau_pt_hist.pdf")




outF.Write()
outF.Close()




print "Finished"