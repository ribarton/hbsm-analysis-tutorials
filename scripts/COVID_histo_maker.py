#!/usr/bin/env python2.7

'''
This script makes histograms from the branchers in intro_tree_maker.py
It creates a ROOT file called sample_histo.root
That stores all of the histograms created in this script

'''

# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"


# fileName = "/cluster/home/amyrewoldt/DAOD/3lep_Hplus500_slep110_Xn165_Xc120/truth1/DAOD_TRUTH1.aod.pool.root"

## Change the file name here, to whatever what output by your ttree flattening file

fileName = "./COVID_data.csv"

file = ROOT.TFile.Open(fileName, "READONLY")

## branch = file.CollectionTree

## the name of the output file that will store the histograms youremakeing
## inthe form of a root file.
outF = ROOT.TFile.Open("COVID_histos.root", "RECREATE")


############################
# Histograms defined Below #
############################

deaths_vs_county = ROOT.TH1F()

tau_pt_hist = ROOT.TH1F("tau_pt_hist","tau_pt",100,0,5e6)

print "Number of input events:", branch.GetEntries()


for entry in xrange(branch.GetEntries()):
  branch.GetEntry(entry)
  taus_container = branch.taus
  jets_container = branch.jets


  for tau in taus_container:
    tau_pt_hist.Fill(tau.Pt())

  for jets in jets_container:
    jet_pt_hist.Fill(jets.Pt())


canvas = ROOT.TCanvas()
tau_pt_hist.Draw("HIST")
canvas.Print("tau_pt_hist.pdf")

jet_pt_hist.Draw("HIST")
canvas.Print("jets_pt_hist.pdf")


outF.Write()
outF.Close()







print "Finished"
