#!/bin/bash

# Path of the directory containing this script
export GIT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

asetup AthDerivation,21.2.101.0,here 
# Sets up a release so that analysis is possible, checkxAOD.py is possible, and so is root after running this
# Try changing this version if things dont work

#Adding scripts to path so they can be executed automatically, you dont have to type "source" or "python" at the beginning of a script
export PATH="${GIT_DIRECTORY}/scripts:${PATH}"

