# HBSM-Analysis-Tutorials

A git repo to get you started with ATLAS analysis
It should be accompanied by this website
https://jcardenas34.github.io/UTA-HBSM-New-Member-Quick-Start-Documentation/

To get started, you should fork your own repository (Top right fork icon in the git page) and start working with that
This will allow you to add to this code in the future if you so choose

then git clone the repository for example
git clone ssh://git@gitlab.cern.ch:7999/your_user_name/hbsm-analysis-tutorials.git

Once you have that, just run 
source setup.sh

and this will setup a release, and you should be able to start analysis.